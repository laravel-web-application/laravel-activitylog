<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Laravel Activity Log
### Things to do list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel-activitylog.git`
2. Go inside the folder: `cd laravel-activitylog`
3. Run `cp .env.example .env` then put your database name & credentials.
4. Run `composer install`
5. Run `php artisan key:generate`
6. Run `php artisan migrate`
7. Run `php artisan serve`
8. Open your favorite browser: http://localhost:8000/add-to-log
9. Open your favorite browser: http://localhost:8000/logActivity

### Screen shot

Add to log

![Add to log](img/add.png "Add to Log")

Log View Page

![Log View Page](img/activity.png "Log View Page")
